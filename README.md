# Smart-lamp-node-red
This repository includes the sources to build a home automation system through the [Node-RED platform](http://nodered.org).

The project consists of 3 main elements:
- **An MQTT broker:** as intermediary of MQTT messages exchanged between connected clients.
- **The application logic:** a flow that instantiates an MQTT client that enables a *publisher* node to turn on, turn off, and make an LED blinking and a *subscriber* node which listen to incoming MQTT messages to show device status updates.
- **A web dashboard:** to interact with the IoT device connected to the MQTT Broker.

By following the instructions below you will be able to run the server locally on your laptop.

## Node-RED
Node-RED is a flow-based programming tool, originally developed by IBM’s Emerging Technology Services team and now a part of the OpenJS Foundation.
Node-RED is a programming tool for wiring together hardware devices, APIs and online services.

Node-RED provides a browser-based flow editor that makes it easy to wire together flows using the wide range of nodes in the palette. Flows can be then deployed to the runtime in a single-click.

JavaScript functions can be created within the editor using a rich text editor.
A built-in library allows you to save useful functions, templates or flows for re-use.

![Node-RED: Low-code programming for event-driven applications](http://nodered.org/images/node-red-screenshot.png)

## Runtime/Editor
Node-RED consists of a Node.js based runtime that you point a web browser at to access the flow editor. Within the browser you create your application by dragging nodes from your palette into a workspace and start to wire them together. With a single click, the application is deployed back to the runtime where it is run.

The palette of nodes can be easily extended by installing new nodes created by the community and the flows you create can be easily shared as JSON files.

# Installation
## Prerequisites
To install Node-RED locally you will need a [supported version of Node.js](https://nodered.org/docs/faq/node-versions).

As the project is using the latest available version of Node-RED (2.2.2), the minimum required Node.JS version is 12.x but **we recommend to install the current Node.JS LTS version 16.x**.

[Here](https://nodejs.org/it/download/) you can download the Node.js source code or a pre-built installer for your platform.

## Quick Start
Once the Node.JS runtime has been installed on your machine and the repository has been successfully cloned, run the following commands inside the project folder:

1. `npm install`
2. `npm run start`
3. Open Node-RED editor Web UI: <http://localhost:1880>
4. Open Node-RED dashboard: <http://localhost:1880/ui>


## How it works
by accessing the dashboard from your browser, the following screen will appear:

![Node-RED Dashboard](./assets/Node-RED-dashboard.png)

There are 4 buttons to interact with the LED:
- **Turn On:** by clicking this button the message "ON" is published to the MQTT broker on the topic */huawei/smartlamp/switch*

- **Turn Off:** by clicking this button the message "OFF" is published to the MQTT broker on the topic */huawei/smartlamp/switch*

- **Start blinking:** by clicking this button an infinite sequence is initialized which publishes "ON" and "OFF" messages at regular intervals to simulate the LED blinking

- **Stop blinking:** by clicking this button the LED blinking sequence is stopped

Next to the buttons there is a light bulb icon that shows the current on / off status of the LED.
Thanks to the MQTT bidirectional communication (publisher/subscriber) the state of this icon gets changed according to the message received both from the Dashboard and from the physical switch button connected to the Raspberry; in this way the icon is always in sync with actual LED state.

## Networking
The IoT device running Oniro should be configured in advanced to connect to the same local network as Node-RED is running. Make sure to setup the configuration files according to the guide provided.

By default the following ports are set when running Node-RED on your local machine:

| Service            | URL Path                    | Port |
|--------------------|-----------------------------|------|
| MQTT Broker        | mqtt://[your-IP-address]    | 1883 |
| Node-RED UI Web Editor | http://[your-IP-address]    | 1880 |
| Node-RED Dashboard | http://[your-IP-address]/ui | 1880 |

## Getting Help

More documentation can be found [here](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/documentation).

For further help, or general discussion, please use the Hackathon slack team.
